FROM ros:melodic-perception
# install packages
RUN apt-get update && apt-get install -y \
    ros-melodic-sensor-msgs \
    && rm -rf /var/lib/apt/lists/

RUN mkdir -p /catkin_ws/src
WORKDIR /catkin_ws/
COPY . src/rtsp_to_image
RUN chmod -R +x src/rtsp_to_image/
RUN /bin/bash -c 'source /opt/ros/melodic/setup.bash && catkin_make'


WORKDIR /
CMD /bin/bash -c "source /opt/ros/melodic/setup.bash; source /catkin_ws/devel/setup.bash; roslaunch rtsp_to_image pub_stream.launch"
