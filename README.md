# rtsp_to_image  

This package is made to receive an RTSP feed from a given IP and publish them as ROS Image/CompressedImage messages on a selected topic.

## Nodes

## convert_stream.py

### Parameters

`~url` (`string`)  
URL of the RTSP video stream.  

`~name` (`string`)  
Name of the node (and the prefix for the published topics)

`~framerate` (`double`)  
Framerate at which to run the node.

`~url_args` (`string`)  
Additional arguments to add to the URL in xml formatting (e.g. `&amp;framerate=5` for the MTGR to run at 29.97/5 fps)

`~reboot` (`bool`)  
Whether or not the VideoCapture object should restart when not receiving frames in order to start receiving again.

### Published Topics

`<name>/image_raw` ([sensor_msgs/Image](http://docs.ros.org/en/api/sensor_msgs/html/msg/Image.html))  
Raw image containing most recent frame received from the RTSP feed.

`<name>/compressed` ([sensor_msgs/CompressedImage](http://docs.ros.org/en/api/sensor_msgs/html/msg/CompressedImage.html))  
Compressed image containing most recent frame received from the RTSP feed.

## Launch files

The included launch files both contain arguments with the same name as the parameters that are sent to the node. The included configurations are:

- `pub_stream.launch`
  - Required arguments:
    - `url`
  - Default arguments:
    - `name = rtsp`
    - `framerate = 30`
    - `url_args = ""`
    - `reboot = true`
- `mtgr.launch`
  - Required arguments:
  - Default arguments:
    - `url = rtsp://192.168.15.233/h264?cam=0`
    - `name = mtgr_stream`
    - `framerate = 5.994`
    - `url_args = "&amp;framerate=5"`
    - `reboot = true`
