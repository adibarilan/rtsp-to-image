#!/usr/bin/env python

import sys, rospy
from bufferless_capture import VideoCapture
from cv_bridge import CvBridge
from sensor_msgs.msg import Image, CompressedImage



def publish_image():
    rospy.init_node('publish_image', anonymous=True)

    url = rospy.get_param("~rtsp_url")
    name = rospy.get_param("~node_name")
    framerate = rospy.get_param("~framerate")
    url_args = rospy.get_param("~url_args")
    reboot = rospy.get_param("~reboot")

    raw_pub = rospy.Publisher(name + '/image_raw', Image, queue_size=1)
    compressed_pub = rospy.Publisher(name + '/compressed', CompressedImage, queue_size=1)

    rate = rospy.Rate(framerate)
    vcap = VideoCapture(url+url_args)
    reboot = reboot

    missed_before = False

    while not rospy.is_shutdown():
        ret, frame = vcap.read(1)
        if ret:
            raw_image_message = CvBridge().cv2_to_imgmsg(frame, encoding="bgr8")
            compressed_image_message = CvBridge().cv2_to_compressed_imgmsg(frame)
            raw_pub.publish(raw_image_message)
            compressed_pub.publish(compressed_image_message)
        else:
            if missed_before and reboot:
                rospy.logerr("Image not received! Restarting VideoCapture...")
                vcap = VideoCapture(url+url_args)
            missed_before = not missed_before
        rate.sleep()

if __name__ == '__main__':
    
    try:
        publish_image()
    except rospy.ROSInterruptException:
        pass

