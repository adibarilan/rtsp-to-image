#!/usr/bin/env python

import cv2, threading, time, os
from Queue import Queue, Empty

class VideoCapture:

    def __init__(self, name):
        os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"
        self.cap = cv2.VideoCapture(name, cv2.CAP_FFMPEG)
        self.q = Queue()
        self.ret = False
        t = threading.Thread(target=self._reader)
        t.daemon = True
        t.start()

    # read frames as soon as they are available, keeping only most recent one
    def _reader(self):
        while True:
            self.ret, frame = self.cap.read()
            if not self.ret:
                continue
            if not self.q.empty():
                try:
                    self.q.get_nowait()  # discard previous (unprocessed) frame
                except:
                    pass
            self.q.put(frame)

    def read(self, timeout):
        try:
            return self.ret, self.q.get(True, timeout)
        except Empty:
            return False, None
            


